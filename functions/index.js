// Require Firebase Libraries
const admin = require('firebase-admin');
const functions = require('firebase-functions');

// External Library
const jsonp = require('node-jsonp');

// Initialize Firebase
admin.initializeApp(functions.config().firebase);

// Export chatBot module to Firebase Cloud
exports.chatBot = functions.database.ref('messages/{messageId}').onWrite(event => {

	// Event Data
	let snapshot = event.data;
	let chat = snapshot.val();

	// Cancel action if data matches a previous one... (Or when it is an `update` event)
	if(snapshot.previous.val()) return;

	// Filter messages when matches a command...
	switch(chat.message.toLowerCase()) {

		case "what is google?":
			return admin.database().ref('messages').push({
				displayName: 'Firebase Bot',
				message: 'Google is AWESOME!'
			});
		break;

		case "show me pictures of cats.":
			return admin.database().ref('messages').push({
				displayName: 'Firebase Bot',
				message: 'Sure! Here you go.<br /><img src="http://lorempixel.com/400/200/cats/" />'
			});
		break;

	}

});

// For more references, check this link: https://firebase.google.com/docs/functions/