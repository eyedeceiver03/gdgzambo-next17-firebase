## Google Cloud Next | Extended Zamboanga
### Firebase Cloud Functions and the Web

![](https://scontent.fmnl10-1.fna.fbcdn.net/v/t1.0-9/18118506_1511661555545544_5160959075797416121_n.jpg?oh=44d57c5599e8c4d98e9b9a22e6dac41d&oe=59C2E90A)

#### Pre-requisites
* NodeJS (https://nodejs.org/en/)

#### How to Setup Firebase CLI (Command-Line Interface)
1. Install `firebase-tools` using NPM.
	* `npm install firebase-tools -g`
2. Type in `firebase login`
	* This will give you a link to login your Google Account.

#### How to create a new project
1. Make sure that you have already created a project.
	* Visit https://console.firebase.google.com/
	* Login and create a project.
2. Create a directory for your project and go to it using your terminal.
	* In this example, we will create it on your **Desktop**
	* Type in `cd Desktop && mkdir firebase-next && cd firebase-next`
	* The line above will go to your Desktop, create a folder, and open it.
3. Type in `firebase init` to initialize a project.
	* For this example, you should consider the following:
		* Choose your project created using the Firebase Console.
		* **DO NOT OVERWRITE** public/index.html
		* **DO NOT OVERWRITE** database.rules.json

#### How to host a local server for testing
1. Type in `firebase serve`.
	* This will host the files under the **public** directory to *localhost:5000*

#### How to deploy your project to Firebase Hosting
1. Type in `firebase deploy`
	* To deploy only the *web app* files without including the **functions** directory, you can do `firebase deploy --except functions`
	* To deploy only the *Firebase Cloud Functions* you can do `firebase deploy --only functions`

------

For questions or inquiries, you can send us a message at the following links:

* [Jhon Andrew](https://m.me/mongsangga)
* [Joshua Bienes](https://m.me/eyedeceiver03)

Or visit us on the following facebook communities:

* [Google Developer Group Zamboanga](https://fb.me/gdgzambo)
* [Zamboanga Programmers | Developers Group](https://www.facebook.com/groups/1590522991241661/)